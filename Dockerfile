FROM node:10-alpine
ENV NODE_ENV production
RUN \
    mkdir /data && \
    echo 'http://dl-cdn.alpinelinux.org/alpine/edge/main' > /etc/apk/repositories && \
    echo 'http://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories && \
    echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories && \
    apk --update add python make g++ npm tmux neovim neomutt && \
    addgroup xterm && \
    adduser -G xterm -S xterm
COPY --chown=xterm:xterm xterm.js /data/src/xterm/
WORKDIR /data/src/xterm
RUN \
    npm install -g radium ajv react webpack concurrently tsc typescript gulp && \
    npm install gulp && \
    npm install -g && \
    npm run build && \
    chown xterm:xterm -R /data/src/xterm
USER xterm
CMD /bin/ash
